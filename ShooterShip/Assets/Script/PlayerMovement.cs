using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D rb;
    [SerializeField] Vector3 mousePos;
    private Vector3 raycast, offset = new Vector3 (0f,0f,10f);
    [SerializeField] float speed, distance;
    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        mousePos = UnityEngine.Camera.main.ScreenToWorldPoint(Input.mousePosition) + offset;
        raycast = mousePos - transform.position;

        distance = raycast.magnitude;

        if (distance > 0.1f)
        {           
            rb.velocity = raycast.normalized * speed;
        }
        else
        {
            rb.velocity = Vector2.zero;
        }
    }
        
        // --------Movement with touch -------------
        // if(Input.touchCount > 0)
        // {
        //     Touch _touch = Input.GetTouch(0);
        //     _touchPosition = Camera.main.ScreenToWorldPoint(_touchPosition);
        //     _touchPosition.z = 0;
        //     _direction = (_touchPosition - transform.position);
        //     _rb.velocity = new Vector2(_direction.x, _direction.y) * _speed;

        //     if(_touch.phase == TouchPhase.Ended)
        //     {
        //         _rb.velocity = Vector2.zero;
        //     }

        // }     
}

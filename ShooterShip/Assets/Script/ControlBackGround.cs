using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBackGround : MonoBehaviour
{
    public Renderer _meshRenderer;
    public float _speed = 0.1f;
    private Vector2 _offset;
    

    // Update is called once per frame
    void Update()
    {
        _meshRenderer.material.mainTextureOffset += new Vector2(0, _speed * Time.deltaTime);
    }
}
